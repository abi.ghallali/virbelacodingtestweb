# Virbela Coding Test #

# Pre-requisites
- Install [Node.js](https://nodejs.org/en/)
- this project used version 16.13.2

# Getting started
- Clone the repository
```
git clone  https://gitlab.com/abi.ghallali/virbelacodingtestweb.git 
```

- Install dependencies
```
cd <virbelacodingtestweb>
cd <Exercise2>
npm install
```
- Build and run the project
```
npm start
```
  Navigate to `http://localhost:5000`

Make sure all dependencies are installed.

# Instructions

- http://localhost:5000/api-docs/ to access to api swagger documentation

- if db.json is not created you can call this API endpoint
http://localhost:5000/building/create
it will create db.json with dummy data

- http://localhost:5000/building/id
this API endpoint we give it the id of the building
and will return building information with all available elevators

- http://localhost:5000/elevator/id
this API endpoint we give it the id of the elevator
and will return elevator information

- http://localhost:5000/elevator/id
this API endpoint updates elevator information
we give it the id of the elevator and the information we want to update in the body
{
    "number": "EL02A-test",
    "doorStatus": true
}

