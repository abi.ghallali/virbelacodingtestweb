//import { v4 as uuid } from 'uuid';
import { Low, JSONFile } from 'lowdb'

// lowdb - create local database
const adapter = new JSONFile('db.json')
const db = new Low(adapter)


// create building dummy data
export const createBuildings = async (req, res, next) => {
    await db.read()
    db.data ||= { 
        buildings: [
            {
                id : 10001,
                number : '1A',
                floorsNum : 12,
                // floorsObj : '', we can add floor object with more details about the floor such as how many apartments...
                address : {
                    address1 : 'adrs 1 street 1...',
                    address1 : '',
                    zipCode : '02456',
                    city : 'Boston', // we can make city object
                    state : 'MA', // we can make state object
                    country : 'United states' // we can make country object
                },
                elevatorsIds: [11101, 11102, 11103]
            },
            {
                id : 10002,
                number : '1B',
                floorsNum : 12,
                address : {
                    address1 : 'adrs 2 street 2...',
                    address1 : '',
                    zipCode : '02456',
                    city : 'Boston', // we can make city object
                    state : 'MA', // we can make state object
                    country : 'United states' // we can make country object
                },
                elevatorsIds: [22201, 22202]
            }
        ],
        elevators: [
            {
                id : 11101,
                number : 'EL01A',
                status : 'stand', // {maintenance, stand, up, Down}
                currentFloor : 0,
                doorStatus : false, // true (open) false (close)
                requestUpList : [],
                requestDownList : []
            },
            {
                id : 11102,
                number : 'EL02A',
                status : 'maintenance', //{maintenance, stand, up, Down}
                currentFloor : 0,
                doorStatus : false, // true (open) false (close)
                requestUpList : [],
                requestDownList : []
            },
            {
                id : 11103,
                number : 'EL03A',
                status : 'stand', //{maintenance, stand, up, Down}
                currentFloor : 0,
                doorStatus : false, // true (open) false (close)
                requestUpList : [],
                requestDownList : []
            },
            {
                id : 22201,
                number : 'EL01B',
                status : 'stand', //{maintenance, stand, up, Down}
                currentFloor : 0,
                doorStatus : false, // true (open) false (close)
                requestUpList : [],
                requestDownList : []
            },
            {
                id : 22202,
                number : 'EL02B',
                status : 'maintenance', //{maintenance, stand, up, Down}
                currentFloor : 0,
                doorStatus : false, // true (open) false (close)
                requestUpList : [],
                requestDownList : []
            }
        ],
    } 
    const { buildings } = db.data
    await db.write();
    res.status(200).send('the example data for the building has been created');
  };



export const getbuilding = async (req, res, next) => {
    // read/get data from db.json
    await db.read();
    const { buildings, elevators } = db.data
    // find build by id
    const buildingObj = buildings.find((building) => {
        if (building.id == req.params.id) {
            // get all available elevators that don't have status maintenance

            // Approach 1
            // this approach not work cause the elevators not mapped in the building.
            // we can use this on mongodb or any non-relational database .
            //const elevatorsArr = building.elevators.filter((elevator) => elevator.status !== 'maintenance');

            // Approach 2
            // we comper building.elevatorsIds (list of Ids) in elevators objects.
            // and we get list of working elevators.
            const elevatorsArr = elevators.filter((elevator) => building.elevatorsIds.indexOf(elevator.id) !== -1 && elevator.status !== 'maintenance')
            delete building.elevatorsIds;
            building.elevators = elevatorsArr
            return building;
        }
    });

    // handling status and sending data
    (buildingObj) ? res.status(200).send(buildingObj) : res.status(404).send("data not found.");
  };