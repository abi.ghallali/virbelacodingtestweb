import { Low, JSONFile } from 'lowdb'

// lowdb - create local database
const adapter = new JSONFile('db.json')
const db = new Low(adapter)


export const getelevator = async (req, res, next) => {
  // read/get data from db.json
  await db.read();
  const { elevators } = db.data
  // find elevator by id
  const elevatorsObj = elevators.find((elevator) => elevator.id == req.params.id);
  // handling status and sending data
  (elevatorsObj) ? res.status(200).send(elevatorsObj) : res.status(404).send("data not found.");
};

export const changeDoorStatus = async (req, res) => {
  // read/get data from db.json
  await db.read();
  
  const { elevators } = db.data
  
  // find elevator by id
  elevators.find((elevator) => {
    if (elevator.id == req.params.id) {
      // here we can get all new properties values defined in the body and updated them
      
      const { number, status, doorStatus, currentFloor } = req.body
      // updated door status
      if(number) elevator.number = number;
      if(status) elevator.status = status;
      if(currentFloor) elevator.currentFloor = currentFloor;
      if(doorStatus === true || doorStatus === false) elevator.doorStatus = doorStatus;
      // (doorStatus) ? elevator.doorStatus = true : elevator.doorStatus = false;
    }
  });
  // save/update file (db.json)
  db.write()

  // in order to show selected elevator data
  const elevatorsObj = elevators.find((elevator) => elevator.id == req.params.id);
  console.log(`the door status has been updated to ${elevatorsObj.doorStatus}. of elevator ${elevatorsObj.id}`);
  res.status(200).send(elevatorsObj);
};