import express from "express";
import bodyParser from "body-parser";

import swaggerJsDoc from "swagger-jsdoc";
import swaggerUI from "swagger-ui-express";


// importing routes
import buildingRoutes from "./routes/building.js";
import elevatorRoutes from "./routes/elevator.js";

const app = express();
const PORT = 5000

// initiate the app with body-parser in order to use JSON on the app
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Swagger Setup
const options = {
	definition: {
		openapi: "3.0.0",
		info: {
			title: "Exercise 2",
			version: "1.0.0",
			description: "Exercise 2 API",
		},
		servers: [
			{
				url: `http://localhost:5000`,
			},
		],
	},
	apis: ["./routes/*.js"],
};
const specs = swaggerJsDoc(options);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

// homepage route
app.get("/", (req, res) => res.send("Welcome to the Virbela Coding Test API!"));

// buildings routes
app.use("/building", buildingRoutes);

// elevators routes
app.use("/elevator", elevatorRoutes);

// error route
app.all("*", (req, res) =>res.send("You've tried reaching a route that doesn't exist."));


app.listen(PORT, () =>console.log(`Server running on port: http://localhost:${PORT}`));