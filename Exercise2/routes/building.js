import express from 'express';

import { createBuildings, getbuilding } from '../controllers/building.js';

const router = express.Router();



// create building to fill database - (for testing)
router.post('/create', createBuildings)
// get all available elevators of building
router.get('/:id', getbuilding);

//router.get('/', () => console.log("buildings"));

export default router;