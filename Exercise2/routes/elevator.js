import express from 'express';

import { getelevator, changeDoorStatus } from '../controllers/elevator.js';

const router = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     Elevator:
 *       type: object
 *       required:
 *         - number
 *         - status
 *         - currentFloor
 *         - doorStatus
 *         - requestUpList
 *         - requestDownList
 *       properties:
 *         number:
 *           type: string
 *           description: The elevator's number
 *         status:
 *           type: string
 *           description: the elevator's status
 *         currentFloor:
 *           type: string
 *           description: The elevator's currentFloor
 *         doorStatus:
 *           type: string
 *           description: the elevator's doorStatus
 *       example:
 *         id: 11101
 */

 /**
  * @swagger
  * tags:
  *   name: Elevators
  *   description: The elevators managing API
  */

 
/**
 * get all available elevators of building
 * 
 * @swagger
 * /elevator/{id}:
 *   get:
 *     summary: Return elevator information by ID
 *     tags: [elevators]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: int
 *         description: enter elevator id.
 *     responses:
 *       200:
 *         description: The elevator information
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Elevator'
 *       404:
 *         description: data not found
 *         content:
 *          application/json:
 *           type: object
 */
router.get('/:id', getelevator);

// change elevator informations

/**
 * @swagger
 * /elevator/{id}:
 *  patch:
 *    summary: update elevator information by ID
 *    tags: [Elevators]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The elevator id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Elevator'
 *    responses:
 *      200:
 *        description: The elevator was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Elevator'
 *      404:
 *        description: The elevator was not found
 *      500:
 *        description: Some error happened
 */
router.patch('/:id', changeDoorStatus);


export default router;